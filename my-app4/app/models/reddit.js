import DS from 'ember-data';

export default DS.Model.extend({
  test: "test",
  title: DS.attr('string'),
  url: DS.attr('string')
});
