import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('my-new-component', { path: '/my-new-component' });
  this.route('reddit', { path: '/reddit/r/:subreddit_name' });
  this.route('blah');
});

export default Router;
