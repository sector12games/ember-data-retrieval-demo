import Ember from 'ember';
//import Dexie from 'npm:dexie';
//import LeafletHeat from 'npm:leaflet.heat';
import LeafletHeat from 'npm:leaflet.heat';
import TwitterNodeClient from 'npm:twitter-node-client';

export default Ember.Component.extend({

    data: [
        [49.72015422,14.27402629,1  ],
        [49.66549949,14.29529302,2  ],
        [49.54634256,14.08849707,3  ]
    ],

    didInsertElement: function() {
        Ember.run.scheduleOnce('afterRender', this.afterRender);
    },
    afterRender: function() {
        var map = L.map('map').setView([51.505, -0.09], 2);

        // Disable drag and zoom handlers.
        //map.dragging.disable();

        //L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        //    maxZoom: 18,
        //    id: 'mapbox.dark',
        //    accessToken: 'pk.eyJ1Ijoic2hhdW5iYXJ1dGgiLCJhIjoiY2lnNWxhZWtzNDZpbXYzbTM3Mndrbzd6ciJ9.YsBrFC23pEcSHDJX8aHQ9g'
        //}).addTo(map);

        L.tileLayer('http://{s}.basemaps.cartocdn.com/dark_nolabels/{z}/{x}/{y}.png',{
            maxZoom: 18,
            minZoom: 2,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',

            // These options apply to the tile layer in the map.
            continuousWorld: false,

            // This option disables loading tiles outside of the world bounds.
            noWrap: true
        }).addTo(map);

        //L.canvasOverlay()
        //    .drawing(drawingOnCanvas)
        //    .addTo(map);

        // Since it's using the HTML5 Canvas API, it is not
        // compatible with IE8 and below. See full documentation:
        // https://www.mapbox.com/mapbox.js/api/v2.2.3/l-tilelayer-canvas/
        var canvasTiles = L.tileLayer.canvas();

        canvasTiles.drawTile = function(canvas, tilePoint, zoom) {
            console.log("drawtile");
            var ctx = canvas.getContext('2d');

            ctx.fillText(tilePoint.toString(), 50, 50);
            ctx.globalAlpha = 0.2;
            ctx.fillStyle = '#000';
            ctx.fillRect(10, 10, 246, 246);

            var centerX = canvas.width / 2;
            var centerY = canvas.height / 2;
            var radius = 70;

            ctx.beginPath();
            ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
            ctx.fillStyle = 'green';
            ctx.fill();
            ctx.lineWidth = 5;
            ctx.strokeStyle = '#003300';
            ctx.stroke();
        };

        //canvasTiles.addTo(map);

        map.on('click', function(e) {
            //alert("Lat, Lon : " + e.latlng.lat + ", " + e.latlng.lng);
        });

        //L.circle([50.5, 30.5], 200).addTo(map);

        addressPoints = addressPoints.map(function (p) { return [p[0], p[1]]; });
        //alert (addressPoints);
        var heat = L.heatLayer(
            addressPoints,
            {
                radius: 25,
                gradient: {0.4: '#BF0040', 0.65: '#D90026', 1: '#FF0000'},
                minOpacity: .5,
                max: 1.2 // (dim the max intensity just a bit - higher numbers dim more)
            }).addTo(map);

        //var Twitter = require('twitter-js-client').Twitter;
        //Get this data from your twitter apps dashboard
        var config = {
            "consumerKey": "VKqP5oKuC9MBeGU7GNlaTvEen",
            "consumerSecret": "FWbI9qZfH7f8ZpISLhFBjSNR1gQuU8Z4b49qXBLBCzRz0NkGRt",
            "accessToken": "36863919-WQI04TOk3Cs3B641OXawwf23sC6S7hXi0NCHe0dUj",
            "accessTokenSecret": "lQpmKHEzehg3VEZkJVDlZI9qlSkRsHYUklHgbGtyijPRJ",
            "callBackUrl": "http://127.0.0.1:4200"
        }

        var twitter = new TwitterNodeClient.Twitter(config);

        twitter.getSearch({'q':'#haiku','count': 10}, error, success);



        //var heat = L.heatLayer([
        //    [50.5, 30.5, 0], // lat, lng, intensity
        //    [50.6, 30.4, 1]
        //], {radius: 2500}).addTo(map);

        //heat.redraw();

    },

    click: function() {
        //var c = document.getElementById("myCanvas");
        //var ctx = c.getContext("2d");
        //ctx.beginPath();
        //ctx.arc(95,50,40,0,2*Math.PI);
        //ctx.stroke();

    },

    drawingOnCanvas(canvasOverlay, params) {
        var data = this.get('data');
        var dot = this.get('dot');

        var ctx = params.canvas.getContext('2d');
        ctx.clearRect(0, 0, params.canvas.width, params.canvas.height);
        ctx.fillStyle = "rgba(255,116,0, 0.2)";
        for (var i = 0; i < data.length; i++) {
            var d = data[i];
            if (params.bounds.contains([d[0], d[1]])) {
                dot = canvasOverlay._map.latLngToContainerPoint([d[0], d[1]]);
                ctx.beginPath();
                ctx.arc(dot.x, dot.y, 3, 0, Math.PI * 2);
                ctx.fill();
                ctx.closePath();
            }
        }
    }
});

//Callback functions
var error = function (err, response, body) {
    console.log('ERROR [%s]', err);
};
var success = function (data) {
    console.log('Data [%s]', data);
};


