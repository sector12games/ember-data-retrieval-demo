**NOTES**
This old project was meant to be the project used as part of my ember blog series.
Unfortunately, I never completed the series, but 1.5 years later I'm now cleaning
out my computer and wanted to commit everything I had. 

- The main project (where I should write future code) is in web/
- I've committed all temp project directories I used when I was tinkering with ember
- I believe my-app4 is the important one, but I'm not sure

There's a new bug introduced with the ember-cli around June 2018. It was an issue
introduced by macos changes (updates to xcode). ember-cli uses node-gyp, which
does some build time stuff that now fails after updating my macos version and
xcode tools. You'll get `make` build errors when running npm install, on any version
of node. node-gyp released a fix for this, but that means you'll need to upgrade
ember-cli in order to use it. Unfortunately this means that all my old ember code
(everything in this repo) can no longer be built until I upgrade to a new version
of ember, or manually change the node-gyp version in package.json. Keep this in
mind if you never need to start this project up again. More info:
- https://github.com/nodejs/node-gyp/issues/809
- https://github.com/nodejs/node-gyp/issues/1454
