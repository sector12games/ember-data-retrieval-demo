import Ember from 'ember';

export default Ember.Route.extend({

  getPublicGitHubRepos: function () {
    var repos = [];
    $.ajax({
      url: "https://api.github.com/repositories",
      type: "GET",
      data: JSON.stringify({})
    }).then(function (response) {
      response.forEach(function (repo) {
        repos.addObject(repo); //fill your array step by step
      });
    });
    return repos;
  },

  model() {
    return this.getPublicGitHubRepos();
  }
});
